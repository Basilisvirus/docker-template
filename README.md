# Docker template

My notes on a basic docker setup application to start a static webpage

## Install Docker
Following [this link](https://docs.docker.com/engine/install/ubuntu/) to install docker on ubuntu, 

- sudo apt-get remove docker docker-engine docker.io containerd runc

- curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

- echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
- sudo apt-get install docker-ce docker-ce-cli containerd.io

- sudo docker run hello-world

## Run simple website 
Following [this video](https://www.youtube.com/watch?v=aUN9Kln8N_k&list=PLovNQ_x19aUgRwQP-ICRs69bqBrE6Ux-V&index=2&ab_channel=Appychip) and [this one](https://www.youtube.com/watch?v=4PvlcTtaAhw&list=PLovNQ_x19aUgRwQP-ICRs69bqBrE6Ux-V&index=3&ab_channel=Appychip)

important:

- sudo docker pull nginx

- sudo usermod -aG docker soldy
